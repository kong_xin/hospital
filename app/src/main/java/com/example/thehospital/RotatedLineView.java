package com.example.thehospital;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @Author LuoGuangMing
 * @Date 2024/4/17
 * @Description
 */
public class RotatedLineView extends RelativeLayout {

    private Paint paint;

    public RotatedLineView(Context context) {
        super(context);
        init();
    }

    public RotatedLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.BLACK); // 设置线条颜色
        paint.setStyle(Paint.Style.STROKE); // 设置为实线
        paint.setStrokeWidth(5f); // 设置线条宽度
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 获取View的中心点
        float centerX = getWidth() / 2f;
        float centerY = getHeight() / 2f;

        // 设置旋转45度
        canvas.rotate(45, centerX, centerY);

        // 绘制一条线，从View的左上角到右下角
        canvas.drawLine(0, 0, getWidth(), getHeight(), paint);
    }
}
