package com.example.thehospital

import android.content.Context
import com.example.thehospital.databinding.DialogJiancezhongBinding
import com.example.thehospital.dialog.BaseDialog

/**
 * @Author LuoGuangMing
 * @Date 2024/5/13
 * @Description 监测中弹框
 */
class JianCeZhongDialog(mContext: Context) : BaseDialog<DialogJiancezhongBinding>(mContext,
    R.layout.dialog_jiancezhong){
    override fun onCreate() {
        setCancelable(false)
        binding.tvCancel.setOnClickListener { dismiss() }
    }
}