package com.example.thehospital.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.thehospital.HzglActivity;
import com.example.thehospital.R;
import com.example.thehospital.OnItemClickListener;
import com.example.thehospital.dialog.ManagerSettingAddDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description 管理员设置
 */
public class ManagerSettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_setting);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        ArrayList<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        Adapter adapter = new Adapter(this, list);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(int position) {
                adapter.setCheck(position);
            }
        });

        findViewById(R.id.ivAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ManagerSettingAddDialog(ManagerSettingActivity.this).show();
            }
        });
    }

    static class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

        static class ViewHolder extends RecyclerView.ViewHolder{

            private LinearLayout llItem;
            private TextView tv1;
            private TextView tv2;
            private TextView tv3;
            private TextView tv4;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                llItem = itemView.findViewById(R.id.llItem);
                tv1 = itemView.findViewById(R.id.tv1);
                tv2 = itemView.findViewById(R.id.tv2);
                tv3 = itemView.findViewById(R.id.tv3);
                tv4 = itemView.findViewById(R.id.tv4);
            }
        }

        private Context mContext;

        private List<String> list;

        public Adapter(Context mContext, List<String> list) {
            this.mContext = mContext;
            this.list = list;
        }

        private int checkPos = 0;


        public void setCheck(int checkPos) {
            this.checkPos = checkPos;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manager_setting,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.llItem.setBackgroundResource(R.drawable.shape_6_white);
            setTextColorDefault(holder);
            if (checkPos == holder.getLayoutPosition()) {
                setTextColorFocus(holder);
                holder.llItem.setBackgroundResource(R.drawable.bg_hzgl_list_item);
            }
            holder.llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(holder.getLayoutPosition());
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        private void setTextColorDefault(ViewHolder holder) {
            holder.tv1.setTextColor(Color.parseColor("#464F60"));
            holder.tv2.setTextColor(Color.parseColor("#464F60"));
            holder.tv3.setTextColor(Color.parseColor("#464F60"));
            holder.tv4.setTextColor(Color.parseColor("#464F60"));
        }

        private void setTextColorFocus(ViewHolder holder) {
            holder.tv1.setTextColor(Color.parseColor("#ffffff"));
            holder.tv2.setTextColor(Color.parseColor("#ffffff"));
            holder.tv3.setTextColor(Color.parseColor("#ffffff"));
            holder.tv4.setTextColor(Color.parseColor("#ffffff"));
        }

        private OnItemClickListener onItemClickListener;

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }
    }
}
