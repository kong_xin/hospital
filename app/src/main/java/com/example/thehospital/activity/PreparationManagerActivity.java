package com.example.thehospital.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thehospital.R;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description 制剂管理
 */
public class PreparationManagerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparation_manager);
    }
}
