package com.example.thehospital.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thehospital.GlszLoginDialog;
import com.example.thehospital.R;
import com.example.thehospital.YyzlActivity;
import com.example.thehospital.ZdbgActivity;
import com.example.thehospital.dialog.HospitalSettingDialog;
import com.example.thehospital.dialog.ManagerSettingAddDialog;
import com.example.thehospital.dialog.OperatorSettingDialog;
import com.example.thehospital.dialog.PatientManagerBuildDialog;
import com.example.thehospital.dialog.PatientManagerEditDialog;
import com.example.thehospital.dialog.QueryStatisticsDialog;
import com.example.thehospital.dialog.SystemSettingDialog;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description
 */
public class MainSecondActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_second);
        findViewById(R.id.second1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GlszLoginDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, ManagerSettingActivity.class));
            }
        });
        findViewById(R.id.second3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, BasisDiseaseActivity.class));
            }
        });
        findViewById(R.id.second4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, DiagnosisDiseaseActivity.class));
            }
        });
        findViewById(R.id.second5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, PreparationManagerActivity.class));
            }
        });
        findViewById(R.id.second16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, PreparationManagerListActivity.class));
            }
        });
        findViewById(R.id.second6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, QueryStatisticsActivity.class));
            }
        });
        findViewById(R.id.second7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ManagerSettingAddDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OperatorSettingDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HospitalSettingDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PatientManagerBuildDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PatientManagerEditDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new QueryStatisticsDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SystemSettingDialog(MainSecondActivity.this).show();
            }
        });
        findViewById(R.id.second14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, YyzlActivity.class));
            }
        });
        findViewById(R.id.second15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainSecondActivity.this, ZdbgActivity.class));
            }
        });
    }
}
