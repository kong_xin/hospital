package com.example.thehospital.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thehospital.R;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description 基础疾病
 */
public class BasisDiseaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basis_disease);
        LinearLayout tvAdd = findViewById(R.id.llAdd);
        LinearLayout llDelete = findViewById(R.id.llDelete);
        LinearLayout llAddViewGroup = findViewById(R.id.llAddViewGroup);
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View itemView = LayoutInflater.from(BasisDiseaseActivity.this)
                        .inflate(R.layout.item_basis_disease, null);
                llAddViewGroup.addView(itemView);
            }
        });
        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llAddViewGroup.getChildCount() > 0) {
                    llAddViewGroup.removeViewAt(llAddViewGroup.getChildCount()-1);
                }
            }
        });
    }
}
