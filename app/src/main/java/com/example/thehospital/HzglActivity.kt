package com.example.thehospital

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import java.util.zip.Inflater

/**
 * @Author LuoGuangMing
 * @Date 2024/5/11
 * @Description
 */
class HzglActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hzgl)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val list = mutableListOf<String>()
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        val adapter = Adapter(this, list)
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener(object : Adapter.OnItemClickListener{
            override fun onClick(position: Int) {
                adapter.setCheck(position)
            }
        })
    }

    class Adapter(private val context: Context,
                  private val list: MutableList<String>) : RecyclerView.Adapter<Adapter.ViewHolder>() {

        class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
            val llItem: LinearLayout = itemView.findViewById(R.id.llItem)
            val tv1: TextView = itemView.findViewById(R.id.tv1)
            val tv2: TextView = itemView.findViewById(R.id.tv2)
            val tv3: TextView = itemView.findViewById(R.id.tv3)
            val tv4: TextView = itemView.findViewById(R.id.tv4)
            val tv5: TextView = itemView.findViewById(R.id.tv5)
            val tv6: TextView = itemView.findViewById(R.id.tv6)
            val tv7: TextView = itemView.findViewById(R.id.tv7)
        }

        private var checkPos = 0

        fun setCheck(checkPos: Int) {
            this.checkPos = checkPos
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_hzgl, null))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.llItem.setBackgroundResource(R.drawable.shape_6_white)
            setTextColorDefault(holder)
            if (checkPos == holder.layoutPosition) {
                setTextColorFocus(holder)
                holder.llItem.setBackgroundResource(R.drawable.bg_hzgl_list_item)
            }
            holder.llItem.setOnClickListener { onItemClickListener?.onClick(holder.layoutPosition) }
        }

        private fun setTextColorDefault(holder: ViewHolder) {
            holder.tv1.setTextColor(Color.parseColor("#464F60"))
            holder.tv2.setTextColor(Color.parseColor("#464F60"))
            holder.tv3.setTextColor(Color.parseColor("#464F60"))
            holder.tv4.setTextColor(Color.parseColor("#464F60"))
            holder.tv5.setTextColor(Color.parseColor("#464F60"))
            holder.tv6.setTextColor(Color.parseColor("#464F60"))
            holder.tv7.setTextColor(Color.parseColor("#464F60"))
        }

        private fun setTextColorFocus(holder: ViewHolder) {
            holder.tv1.setTextColor(Color.parseColor("#ffffff"))
            holder.tv2.setTextColor(Color.parseColor("#ffffff"))
            holder.tv3.setTextColor(Color.parseColor("#ffffff"))
            holder.tv4.setTextColor(Color.parseColor("#ffffff"))
            holder.tv5.setTextColor(Color.parseColor("#ffffff"))
            holder.tv6.setTextColor(Color.parseColor("#ffffff"))
            holder.tv7.setTextColor(Color.parseColor("#ffffff"))
        }

        private var onItemClickListener: OnItemClickListener ?= null

        fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
            this.onItemClickListener = onItemClickListener
        }

        interface OnItemClickListener {
            fun onClick(position: Int)
        }
    }
}