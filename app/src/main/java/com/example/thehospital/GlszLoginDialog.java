package com.example.thehospital;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.thehospital.databinding.DialogGlszBinding;
import com.example.thehospital.dialog.BaseDialogJava;


/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description 进入管理设置
 */
public class GlszLoginDialog extends BaseDialogJava<DialogGlszBinding> {

    public GlszLoginDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void onCreate() {
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected int getRes() {
        return R.layout.dialog_glsz;
    }
}
