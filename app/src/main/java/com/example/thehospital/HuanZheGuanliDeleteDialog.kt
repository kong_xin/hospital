package com.example.thehospital

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import com.example.thehospital.databinding.DialogHzglBjscBinding
import com.example.thehospital.dialog.BaseDialog

/**
 * @Author LuoGuangMing
 * @Date 2024/5/13
 * @Description 患者管理弹框
 */
class HuanZheGuanliDeleteDialog(mContext: Context) : BaseDialog<DialogHzglBjscBinding>(mContext,
    R.layout.dialog_hzgl_bjsc){
    override fun onCreate() {

    }

    override fun show() {
        super.show()
        //设置弹出位置
        val window = window
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val params = window!!.attributes
        params.gravity = Gravity.CENTER
        window.attributes = params
    }
}