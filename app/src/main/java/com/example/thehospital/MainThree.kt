package com.example.thehospital

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.thehospital.three.Jbzd2Activity
import com.example.thehospital.three.JbzdActivity
import com.example.thehospital.three.PatientManagerBuild2Dialog
import com.example.thehospital.three.Yyzl2Activity
import com.example.thehospital.three.ZdbgThreeActivity

/**
 * @Author LuoGuangMing
 * @Date 2024/4/16
 * @Description
 */
class MainThree: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_three)
        findViewById<TextView>(R.id.tv1).setOnClickListener {
            startActivity(Intent(this, ZdbgThreeActivity::class.java))
        }
        findViewById<TextView>(R.id.tv2).setOnClickListener {
            startActivity(Intent(this, JbzdActivity::class.java))
        }
        findViewById<TextView>(R.id.tv3).setOnClickListener {
            startActivity(Intent(this, Jbzd2Activity::class.java))
        }
        findViewById<TextView>(R.id.tv4).setOnClickListener {
            startActivity(Intent(this, Yyzl2Activity::class.java))
        }
        findViewById<TextView>(R.id.tv5).setOnClickListener {
            PatientManagerBuild2Dialog(this).show()
        }
    }
}