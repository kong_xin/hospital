package com.example.thehospital

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import com.example.thehospital.databinding.DialogHzglNewBuildBinding
import com.example.thehospital.dialog.BaseDialog

/**
 * @Author LuoGuangMing
 * @Date 2024/5/13
 * @Description 患者管理弹框
 */
class HuanZheGuanliDialog(mContext: Context) : BaseDialog<DialogHzglNewBuildBinding>(mContext,
    R.layout.dialog_hzgl_new_build){
    override fun onCreate() {
        binding.ivClose.setOnClickListener { dismiss() }
    }

    override fun show() {
        super.show()
        //设置弹出位置
        val window = window
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val params = window!!.attributes
        params.gravity = Gravity.CENTER
        window.attributes = params
    }
}