package com.example.thehospital

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * @Author LuoGuangMing
 * @Date 2024/4/16
 * @Description
 */
class Main: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        findViewById<TextView>(R.id.tv1).setOnClickListener {
            startActivity(Intent(this, ZkcsActivity::class.java))
        }
        findViewById<TextView>(R.id.tv2).setOnClickListener {
            startActivity(Intent(this, ZdbgActivity::class.java))
        }
        findViewById<TextView>(R.id.tv3).setOnClickListener {
            startActivity(Intent(this, FmActivity::class.java))
        }
        findViewById<TextView>(R.id.tv4).setOnClickListener {
            startActivity(Intent(this, Fm2Activity::class.java))
        }
        findViewById<TextView>(R.id.tv5).setOnClickListener {
            startActivity(Intent(this, GlszActivity::class.java))
        }
        findViewById<TextView>(R.id.tv6).setOnClickListener {
            startActivity(Intent(this, HzglActivity::class.java))
        }
        findViewById<TextView>(R.id.tv7).setOnClickListener {
            startActivity(Intent(this, YyzlActivity::class.java))
        }
        findViewById<TextView>(R.id.tv8).setOnClickListener {
            GlszDialog(this).show()
        }
        findViewById<TextView>(R.id.tv9).setOnClickListener {
            HuanZheGuanliDialog(this).show()
        }
        findViewById<TextView>(R.id.tv10).setOnClickListener {
            HuanZheGuanliDeleteDialog(this).show()
        }
        findViewById<TextView>(R.id.tv11).setOnClickListener {
            JianCeZhongDialog(this).show()
        }
    }
}