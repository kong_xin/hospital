package com.example.thehospital;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description
 */
public interface OnItemClickListener {
    void onClick(int position);
}
