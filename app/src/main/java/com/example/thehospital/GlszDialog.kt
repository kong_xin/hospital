package com.example.thehospital

import android.content.Context
import com.example.thehospital.databinding.DialogSettingBinding
import com.example.thehospital.dialog.BaseDialog

/**
 * @Author LuoGuangMing
 * @Date 2024/5/13
 * @Description 管理设置弹框
 */
class GlszDialog(mContext: Context) : BaseDialog<DialogSettingBinding>(mContext, R.layout.dialog_setting){
    override fun onCreate() {
    }
}