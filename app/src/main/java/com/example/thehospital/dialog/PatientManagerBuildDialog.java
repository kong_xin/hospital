package com.example.thehospital.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.thehospital.R;
import com.example.thehospital.databinding.DialogHzglNewBuildBinding;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/14
 * @Description 患者管理新建
 */
public class PatientManagerBuildDialog extends BaseDialogJava<DialogHzglNewBuildBinding>{

    public PatientManagerBuildDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate() {
        setCancelable(false);
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected int getRes() {
        return R.layout.dialog_hzgl_new_build;
    }
}
