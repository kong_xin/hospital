package com.example.thehospital.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.thehospital.R


/**
 * @Author LuoGuangMing
 * @Date 2023/2/22 9:57
 * @Description
 */
abstract class BaseDialog<T : ViewDataBinding>(thisContext: Context, val res: Int, style: Int = R.style.myDialog):
    Dialog(thisContext, style){

    lateinit var binding : T

    abstract fun onCreate()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), res, null, false )
        setContentView(binding.root)
        onCreate()
    }

    override fun show() {
        super.show()
        //设置弹出位置
        val window = window
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val params = window!!.attributes
//        val width = context.getScreenWidth() - context.dip2px(70f)
//        params.width = width
        params.gravity = Gravity.CENTER
        window.attributes = params
    }
}