package com.example.thehospital.dialog;

import android.content.Context;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;

import com.example.thehospital.R;
import com.example.thehospital.databinding.DialogSystemSettingBinding;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/14
 * @Description 系统设置
 */
public class SystemSettingDialog extends BaseDialogJava<DialogSystemSettingBinding>{

    public SystemSettingDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate() {
        setCancelable(false);
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        binding.sbVoice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.tvProgressVoice.setText(""+i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        binding.sbBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.tvProgressBrightness.setText(""+i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected int getRes() {
        return R.layout.dialog_system_setting;
    }
}
