package com.example.thehospital.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.example.thehospital.R;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/13
 * @Description
 */
abstract public class BaseDialogJava<T extends ViewDataBinding> extends Dialog {

    protected T binding;

    private final Context mContext;

    public BaseDialogJava(@NonNull Context context) {
        super(context, R.style.myDialog);
        mContext = context;
    }

    abstract protected void onCreate();

    abstract protected int getRes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), getRes(), null, false );
        setContentView(binding.getRoot());
        onCreate();
    }

    @Override
    public void show() {
        super.show();
        //设置弹出位置
        Window window = getWindow();
        window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams attributes = window.getAttributes();

        attributes.gravity = Gravity.CENTER;
        window.setAttributes(attributes);
    }
}
