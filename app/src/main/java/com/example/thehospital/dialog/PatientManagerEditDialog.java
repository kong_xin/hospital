package com.example.thehospital.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.thehospital.R;
import com.example.thehospital.databinding.DialogHzglBjscBinding;
import com.example.thehospital.databinding.DialogHzglNewBuildBinding;

/**
 * @Author LuoGuangMing
 * @Date 2024/6/14
 * @Description 患者管理编辑删除
 */
public class PatientManagerEditDialog extends BaseDialogJava<DialogHzglBjscBinding>{

    public PatientManagerEditDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate() {
        setCancelable(false);
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected int getRes() {
        return R.layout.dialog_hzgl_bjsc;
    }
}
